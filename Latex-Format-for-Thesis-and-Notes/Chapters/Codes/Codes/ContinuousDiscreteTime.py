import matplotlib.pyplot
import numpy

t=numpy.arange(0,2*numpy.pi, 0.1)
y=numpy.sin(t)

z=numpy.zeros((len(y)))
              
for i in range(len(y)):
    if y[i]<1 and y[i]>0.5:
        z[i]=1
    elif y[i]>0 and y[i]<0.5:
        z[i]=0.5
    elif y[i]<0 and y[i]>-0.5:
        z[i]=-0.5
    else:
        z[i]=-1

matplotlib.pyplot.figure(0)
#plot: Continuous Time Continuous Amplitude plot
matplotlib.pyplot.plot(t,y)
matplotlib.pyplot.xlabel("Continuous Time")
matplotlib.pyplot.ylabel("Continuous Amplitude")
matplotlib.pyplot.title('Continuous Time Continuous Amplitude')
matplotlib.pyplot.xlim([0, max(t)])
matplotlib.pyplot.ylim([-1.5, 1.5])

matplotlib.pyplot.figure(1)
#step: Discrete Time Discrete Amplitude signal
matplotlib.pyplot.step(t,z) 
matplotlib.pyplot.xlabel("Discrete Time")
matplotlib.pyplot.ylabel("Discrete Amplitude")
matplotlib.pyplot.title('Continuous Time Discrete Amplitude')
matplotlib.pyplot.ylim([-1.5, 1.5])

matplotlib.pyplot.figure(2)
#stem: Continuous Time Discrete Amplitude signal
matplotlib.pyplot.stem(z) 
matplotlib.pyplot.xlabel("Discrete Time")
matplotlib.pyplot.ylabel("Discrete Amplitude")
matplotlib.pyplot.title('Discrete Time Discrete Amplitude')
matplotlib.pyplot.xlim(0, len(t))
matplotlib.pyplot.ylim([-1.5, 1.5])

matplotlib.pyplot.figure(3)
matplotlib.pyplot.stem(y) 
matplotlib.pyplot.xlabel("Discrete Time")
matplotlib.pyplot.ylabel("Discrete Amplitude")
title2 =matplotlib.pyplot.title('Discrete Time Continuous Amplitude')
matplotlib.pyplot.setp(title2, color='r') 

matplotlib.pyplot.show()
