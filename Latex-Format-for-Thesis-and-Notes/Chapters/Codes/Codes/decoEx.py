from deco import funcName

class Integer: 
	def __init__(self, parameter):
		self.parameter = parameter

	# __get__ is red in comment
	@funcName
	def __get__(self, instance, cls):
		# print("get")
		if instance is None: 
			return self
		else: 
			return instance.__dict__[self.parameter]

	def __set__(self, instance, value):
		print("setting %s to %s" % (self.parameter, value))
		if not isinstance(value, int):
			raise TypeError("Interger value is expected")
		instance.__dict__[self.parameter] = value

class Rect: 
	length = Integer('length')
	width =  Integer('width')

	def __init__(self, length, width):
		self.length = length
		self.width = width

r = Rect(2,1)
print(r.length)

r = Rect(3, 1.5)